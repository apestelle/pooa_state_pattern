Voici un exemple d'implémentation du State pattern.

Cet exemple utilise un lecteur de vidéo.
Ce lecteur possède plusieurs états et nécessite des traitements différents en fonction des états. 
Un changement du comportement est nécessaire en fonction de l'état.

On trouve deux packages dans le projet.

Le premier BadImplement montre une mauvaise implémentation basée sur des traitements conditionnels.

Le second StatePattern utilise le patron état afin d'implémenter la solution. 


Les traitements finaux sont similaires, cependant le package StatePattern est plus facilement maintenable et est plus facilement compréhensible.