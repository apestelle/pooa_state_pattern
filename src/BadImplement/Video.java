package BadImplement;

public class Video {
    private String etat;

    public Video(){
        this.etat = new String();
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public void action(){
        if(etat.equalsIgnoreCase("PLAY")){
            System.out.println("La vidéo est en lecture");
        }else if(etat.equalsIgnoreCase("PAUSE")){
            System.out.println("La vidéo est en pause");
        }
    }
}


//Ici on peut implémenter notre lecteur vidéo de cette manière simple. Elle fonctionne mais est difficilement maintenable.
//Le traitement conditionnel effectué dans la classe action implique d'ajouter des blocs de codes afin de gérer les traitements.
//Chaque nouvel état implique d'ajouter un nouveau bloc else if
//On obtient une classe surchargée, nécessitant un temps important afin de comprendre son fonctionnement