package StatePattern;

public class Main {
    public static void main(String[] args){
        Video video = new Video();
        video.setState(new PlayVideoState());
        video.handle();
        video.setState(new PauseVideoState());
        video.handle();
    }
}
