package StatePattern;

interface VideoState {
    void handle(Video context);
}
