package StatePattern;

public class Video {
    private VideoState videoState;

    public void setState(VideoState newEtat) {
        this.videoState = newEtat;
    }

    public void handle() {
        videoState.handle(this);
    }
}
