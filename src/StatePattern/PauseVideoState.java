package StatePattern;

class PauseVideoState implements VideoState {
    @Override
    public void handle(Video context) {
        System.out.println("Break;");
    }
}
