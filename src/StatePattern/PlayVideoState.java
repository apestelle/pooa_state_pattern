package StatePattern;

class PlayVideoState implements VideoState {
    @Override
    public void handle(Video context) {
        System.out.println("Play;");
    }
}
