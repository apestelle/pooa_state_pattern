package StatePattern;

class BackToStart implements VideoState {
    @Override
    public void handle(Video context) {
        System.out.println("Restart;");
    }
}